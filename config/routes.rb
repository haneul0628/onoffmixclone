Rails.application.routes.draw do
  resources :projects
  get 'static_pages/about'
  root 'static_pages#about' #처음페이지를 about로 띄운다는뜻
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
